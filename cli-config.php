<?php

require 'vendor/autoload.php';

use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\Configuration\Migration\ConfigurationArray;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManager;
use Verifarma\Api\ApiKernel;

$config = [
    'migrations_paths' => [
        'DoctrineMigrations' => './migrations'
    ],
];


$kernel = new ApiKernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$kernel->boot();

$entityManager = $kernel->getContainer()->get(EntityManager::class);

return DependencyFactory::fromEntityManager(new ConfigurationArray($config), new ExistingEntityManager($entityManager));
<?php


declare(strict_types=1);

namespace Verifarma\Tests\Shared\Infrastructure\Doctrine;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Verifarma\Api\ApiKernel;

abstract class InfrastructureTestCase extends KernelTestCase
{
	protected function service(string $id): mixed
	{
		return $this->getContainer()->get($id);
	}

	public static function getKernelClass(): string
	{
		return ApiKernel::class;
	}
}

<?php

declare(strict_types=1);

namespace Verifarma\Tests\Pharmacies\Infrastructure\Persistence;

use Doctrine\ORM\EntityManager;
use Verifarma\Pharmacies\Domain\Coordinates;
use Verifarma\Pharmacies\Domain\Pharmacy;
use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Pharmacies\Infrastructure\Persistence\DoctrinePharmacyRepository;
use Verifarma\Tests\Pharmacies\Domain\CoordinatesMother;
use Verifarma\Tests\Pharmacies\Domain\PharmacyMother;
use Verifarma\Tests\Shared\Infrastructure\Doctrine\InfrastructureTestCase;

final class DoctrinePharmacyRepositoryTest extends InfrastructureTestCase
{
	/**
	 * @test
	 */
	public function it_should_return_nearest_pharmacy(): void
	{
		/** @var EntityManager $entityManager */
		$entityManager = $this->service(EntityManager::class);
		$entityManager->getConnection()->exec('TRUNCATE TABLE pharmacies');

		$repository = new DoctrinePharmacyRepository($entityManager);

		$patientCoordinates = CoordinatesMother::create();

		$createdPharmacies = [];

		while (count($createdPharmacies) < 10) {
			$pharmacy = PharmacyMother::create();
			$repository->save($pharmacy);

			$createdPharmacies[] = $pharmacy;
		}

		$expectedPharmacyId = $this->getNearestPharmacyId($patientCoordinates, ...$createdPharmacies);
		$nearestPharmacy = $repository->findNearest($patientCoordinates);

		$this->assertEquals($nearestPharmacy->getId(), $expectedPharmacyId);
	}

	public static function getNearestPharmacyId(Coordinates $patientCoordinates, Pharmacy ...$pharmacies): PharmacyId
	{
		$distances = [];

		foreach ($pharmacies as $pharmacy) {
			$key = $pharmacy->getId()->value();
			$distances[$key] = self::calculateDistance($pharmacy->getCoordinates(), $patientCoordinates);
		}

		asort($distances);

		return PharmacyId::create(array_key_first($distances));
	}

	public static function calculateDistance(Coordinates $pharmacyCoordinates, Coordinates $clientCoordinates): float
	{
		$lat1 = $pharmacyCoordinates->getLatitude()->value();
		$lon1 = $pharmacyCoordinates->getLongitude()->value();

		$lat2 = $clientCoordinates->getLatitude()->value();
		$lon2 = $clientCoordinates->getLongitude()->value();

		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);

		return $dist * 60 * 1.1515;
	}
}

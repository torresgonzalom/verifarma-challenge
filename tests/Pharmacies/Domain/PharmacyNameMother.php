<?php

declare(strict_types=1);

namespace Verifarma\Tests\Pharmacies\Domain;

use Verifarma\Pharmacies\Domain\PharmacyName;
use Verifarma\Tests\Shared\Domain\MotherCreator;

final class PharmacyNameMother
{
	public static function create(?string $value = null): PharmacyName
	{
		return PharmacyName::create($value ?? MotherCreator::random()->company());
	}
}

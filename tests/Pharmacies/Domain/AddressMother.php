<?php

declare(strict_types=1);

namespace Verifarma\Tests\Pharmacies\Domain;

use Verifarma\Pharmacies\Domain\Address;
use Verifarma\Tests\Shared\Domain\MotherCreator;

final class AddressMother
{
	public static function create(?string $value = null): Address
	{
		return Address::create($value ?? MotherCreator::random()->address());
	}
}

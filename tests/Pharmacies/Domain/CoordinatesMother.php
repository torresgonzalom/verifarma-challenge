<?php

declare(strict_types=1);

namespace Verifarma\Tests\Pharmacies\Domain;

use Verifarma\Pharmacies\Domain\Coordinates;
use Verifarma\Pharmacies\Domain\Latitude;
use Verifarma\Pharmacies\Domain\Longitude;
use Verifarma\Tests\Shared\Domain\MotherCreator;

final class CoordinatesMother
{
	public static function create(?float $latitude = null, ?float $longitude = null): Coordinates
	{
		return Coordinates::create(
			Latitude::create($latitude ?? MotherCreator::random()->latitude()),
			Longitude::create($longitude ?? MotherCreator::random()->longitude()),
		);
	}
}

<?php

declare(strict_types=1);

namespace Verifarma\Tests\Pharmacies\Domain;

use Verifarma\Pharmacies\Domain\Address;
use Verifarma\Pharmacies\Domain\Coordinates;
use Verifarma\Pharmacies\Domain\Pharmacy;
use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Pharmacies\Domain\PharmacyName;

final class PharmacyMother
{
	public static function create(
		?PharmacyId $pharmacyId = null,
		?PharmacyName $pharmacyName = null,
		?Address $pharmacyAddress = null,
		?Coordinates $coordinates = null,
	): Pharmacy {
		return Pharmacy::create(
			$pharmacyId ?? PharmacyIdMother::create(),
			$pharmacyName ?? PharmacyNameMother::create(),
			$pharmacyAddress ?? AddressMother::create(),
			$coordinates ?? CoordinatesMother::create(),
		);
	}
}

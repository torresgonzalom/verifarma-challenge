<?php

declare(strict_types=1);

namespace Verifarma\Tests\Pharmacies\Domain;

use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Tests\Shared\Domain\MotherCreator;

final class PharmacyIdMother
{
	public static function create(?string $value = null): PharmacyId
	{
		return PharmacyId::create($value ?? MotherCreator::random()->uuid());
	}
}

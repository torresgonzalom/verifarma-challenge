<?php

declare(strict_types=1);

namespace Verifarma\Tests\Pharmacies\Application\create;

use Verifarma\Pharmacies\Application\Create\CreatePharmacyCommand;
use Verifarma\Pharmacies\Domain\Pharmacy;
use Verifarma\Tests\Pharmacies\Domain\AddressMother;
use Verifarma\Tests\Pharmacies\Domain\CoordinatesMother;
use Verifarma\Tests\Pharmacies\Domain\PharmacyIdMother;
use Verifarma\Tests\Pharmacies\Domain\PharmacyNameMother;

final class CreatePharmacyCommandMother
{
	public static function create(
		?string $id = null,
		?string $name = null,
		?string $address = null,
		?float $latitude = null,
		?float $longitude = null,
	): CreatePharmacyCommand {
		return new CreatePharmacyCommand(
			$id ?? PharmacyIdMother::create()->value(),
			$name ?? PharmacyNameMother::create()->value(),
			$address ?? AddressMother::create()->value(),
			$latitude ?? CoordinatesMother::create()->getLatitude()->value(),
			$longitude ?? CoordinatesMother::create()->getLongitude()->value(),
		);
	}

	public static function createFromPharmacy(Pharmacy $pharmacy): CreatePharmacyCommand
	{
		return new CreatePharmacyCommand(
			$pharmacy->getId()->value(),
			$pharmacy->getName()->value(),
			$pharmacy->getAddress()->value(),
			$pharmacy->getCoordinates()->getLatitude()->value(),
			$pharmacy->getCoordinates()->getLongitude()->value(),
		);
	}
}

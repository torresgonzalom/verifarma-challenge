<?php

declare(strict_types=1);

namespace Verifarma\Tests\Pharmacies\Application\create;

use Hamcrest\Core\IsEqual;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Verifarma\Pharmacies\Application\Create\CreatePharmacyCommandHandler;
use Verifarma\Pharmacies\Application\Create\PharmacyCreator;
use Verifarma\Pharmacies\Domain\Exception\PharmacyIdAlreadyExists;
use Verifarma\Pharmacies\Domain\PharmacyRepository;
use Verifarma\Tests\Pharmacies\Domain\PharmacyMother;

final class CreatePharmacyCommandHandlerTest extends MockeryTestCase
{
	/**
	 * @test
	 */
	public function it_should_create_a_pharmacy(): void
	{
		$repository = Mockery::mock(PharmacyRepository::class);
		$pharmacy = PharmacyMother::create();

		$repository->shouldReceive('find')
			->with(IsEqual::equalTo($pharmacy->getId()))
			->once()
			->andReturnNull();

		$repository->shouldReceive('save')
			->with(IsEqual::equalTo($pharmacy))
			->once()
			->andReturnNull();

		$pharmacyCreator = new PharmacyCreator($repository);
		$handler = new CreatePharmacyCommandHandler($pharmacyCreator);

		$command = CreatePharmacyCommandMother::createFromPharmacy($pharmacy);

		$handler($command);
	}

	/**
	 * @test
	 */
	public function it_should_throw_a_pharmacy_already_exists_exception(): void
	{
		$repository = Mockery::mock(PharmacyRepository::class);
		$pharmacy = PharmacyMother::create();

		$repository->shouldReceive('find')
			->with(IsEqual::equalTo($pharmacy->getId()))
			->once()
			->andReturn($pharmacy);

		$repository->shouldNotReceive('save');

		$pharmacyCreator = new PharmacyCreator($repository);
		$handler = new CreatePharmacyCommandHandler($pharmacyCreator);

		$command = CreatePharmacyCommandMother::create(id: $pharmacy->getId()->value());

		$this->expectException(PharmacyIdAlreadyExists::class);

		$handler($command);
	}
}

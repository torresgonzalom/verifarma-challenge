<h1 align="center">
    Verifarma tech lead challenge
</h1>

## Environment Setup

### Needed tools

1. [Install Docker](https://www.docker.com/get-started)
2. [Install Docker compose](https://docs.docker.com/compose/install/)
3. Clone this project: `git clone git@gitlab.com:torresgonzalom/verifarma-challenge.git verifarma-challenge`
4. Move to the project folder: `cd verifarma-challenge`

### Application execution

1. Create environment file: `cp .env.example .env`
2. Install dependencies executing: `docker-compose run --no-deps --rm api composer install`
3. Init local env: `docker-compose run --rm api composer init-local-env`
4. Run application: `docker-compose up -d`
5. Check app status: http://localhost:8080/health-check
6. API Docs: http://localhost:8080/openapi.html

### Tests and linting execution

1. Execute unit tests: `docker-compose exec composer unit-tests`
2. Execute static analysis: `docker-compose exec composer psalm`
3. Execute architecture tests: `docker-compose exec composer phpat`
4. Execute code style check: `docker-compose exec composer ecs`
5. Fix code style: `docker-compose exec composer ecs-fix`
6. Execute all tests and linting before commit: `docker-compose exec composer pre-commit-validations`

<?php

declare(strict_types=1);

namespace Verifarma\Api\Controller\HealthCheck;

use Symfony\Component\HttpFoundation\JsonResponse;

final class HealthCheckGetController
{
	public function __invoke(): JsonResponse
	{
		return new JsonResponse(
			[
				'api-status' => 'ok',
			]
		);
	}
}

<?php

namespace Verifarma\Api\Controller\Pharmacies;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Verifarma\Pharmacies\Application\Find\GetNearestPharmacyQuery;
use Verifarma\Pharmacies\Application\Find\GetPharmacyResponse;
use Verifarma\Pharmacies\Domain\Exception\InvalidLatitude;
use Verifarma\Pharmacies\Domain\Exception\InvalidLongitude;
use Verifarma\Shared\Infrastructure\Symfony\ApiController;
use function Lambdish\Phunctional\filter;

class NearestPharmaciesGetController extends ApiController
{
    public function __invoke(Request $request): JsonResponse
    {
        $query = new GetNearestPharmacyQuery($request->get('lat', 0), $request->get('lon', 0));

        /** @var GetPharmacyResponse $pharmacyResponse */
        $pharmacyResponse = $this->ask($query);

        return $this->successResponse([
            'type' => 'pharmacies',
            'id' => $pharmacyResponse->pharmacy['id'],
            'attributes' => filter(fn($value, $key) => $key != 'id', $pharmacyResponse->pharmacy),
        ]);
    }

    protected function exceptions(): array
    {
        return [
            InvalidLatitude::class => Response::HTTP_UNPROCESSABLE_ENTITY,
            InvalidLongitude::class => Response::HTTP_UNPROCESSABLE_ENTITY,
        ];
    }
}
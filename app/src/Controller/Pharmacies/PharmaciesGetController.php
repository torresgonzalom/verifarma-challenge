<?php

namespace Verifarma\Api\Controller\Pharmacies;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Verifarma\Pharmacies\Application\Find\GetPharmacyQuery;
use Verifarma\Pharmacies\Application\Find\GetPharmacyResponse;
use Verifarma\Pharmacies\Domain\Exception\PharmacyNotFound;
use Verifarma\Shared\Domain\ValueObject\Exception\InvalidUuid;
use Verifarma\Shared\Infrastructure\Symfony\ApiController;
use function Lambdish\Phunctional\filter;

class PharmaciesGetController extends ApiController
{
    public function __invoke(string $id): JsonResponse
    {
        /** @var GetPharmacyResponse $pharmacyResponse */
        $pharmacyResponse = $this->ask(new GetPharmacyQuery($id));

        return $this->successResponse([
            'type' => 'pharmacies',
            'id' => $pharmacyResponse->pharmacy['id'],
            'attributes' => filter(fn($value, $key) => $key != 'id', $pharmacyResponse->pharmacy),
        ]);
    }

    protected function exceptions(): array
    {
        return [
            PharmacyNotFound::class => Response::HTTP_NOT_FOUND,
            InvalidUuid::class => Response::HTTP_UNPROCESSABLE_ENTITY,
        ];
    }
}
<?php

namespace Verifarma\Api\Controller\Pharmacies;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Verifarma\Pharmacies\Application\Create\CreatePharmacyCommand;
use Verifarma\Pharmacies\Domain\Exception\InvalidAddressLength;
use Verifarma\Pharmacies\Domain\Exception\InvalidLatitude;
use Verifarma\Pharmacies\Domain\Exception\InvalidLongitude;
use Verifarma\Pharmacies\Domain\Exception\InvalidPharmacyNameLength;
use Verifarma\Pharmacies\Domain\Exception\PharmacyIdAlreadyExists;
use Verifarma\Shared\Domain\ValueObject\Exception\InvalidUuid;
use Verifarma\Shared\Infrastructure\Symfony\ApiController;

class PharmaciesPostController extends ApiController
{
    public function __invoke(Request $request): JsonResponse
    {
        $data = $request->get('data');

        $command = new CreatePharmacyCommand(
            $request->get('id', ''),
            $data['name'],
            $data['address'],
            $data['latitude'],
            $data['longitude'],
        );

        $this->dispatch($command);

        return $this->successResponse([], Response::HTTP_CREATED);
    }

    protected function exceptions(): array
    {
        return [
            PharmacyIdAlreadyExists::class => Response::HTTP_UNPROCESSABLE_ENTITY,
            InvalidUuid::class => Response::HTTP_UNPROCESSABLE_ENTITY,
            InvalidLatitude::class => Response::HTTP_UNPROCESSABLE_ENTITY,
            InvalidLongitude::class => Response::HTTP_UNPROCESSABLE_ENTITY,
            InvalidAddressLength::class => Response::HTTP_UNPROCESSABLE_ENTITY,
            InvalidPharmacyNameLength::class => Response::HTTP_UNPROCESSABLE_ENTITY,
        ];
    }
}
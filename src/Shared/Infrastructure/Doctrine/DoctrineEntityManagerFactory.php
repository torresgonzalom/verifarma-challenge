<?php

declare(strict_types=1);

namespace Verifarma\Shared\Infrastructure\Doctrine;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Schema\AbstractAsset;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\SimplifiedXmlDriver;
use Doctrine\ORM\ORMSetup;
use Jsor\Doctrine\PostGIS\Event\ORMSchemaEventSubscriber;
use Verifarma\Shared\Infrastructure\Doctrine\Dbal\DbalCustomTypesRegistrar;

final class DoctrineEntityManagerFactory
{
	public static function create(array $parameters, string $environment): EntityManager
	{
		$isDevMode = $environment !== 'prod';

		$prefixes = array_merge(DoctrinePrefixesSearcher::inPath(__DIR__ . '/../../../../src', 'Verifarma'),);

		$dbalCustomTypesClasses = DbalTypesSearcher::inPath(__DIR__ . '/../../../../src');

		DbalCustomTypesRegistrar::register($dbalCustomTypesClasses);

		$config = self::createConfiguration($prefixes, $isDevMode);

		$config->setSchemaAssetsFilter(static function (object|string $assetName): bool {
			if ($assetName instanceof AbstractAsset) {
				$assetName = $assetName->getName();
			}
			/** @var string $assetName */
			return (bool) preg_match('/^(?!tiger)(?!topology)/', $assetName);
		});

		$entityManager = new EntityManager(DriverManager::getConnection($parameters, $config, new EventManager()), $config);

		$entityManager->getEventManager()->addEventSubscriber(new ORMSchemaEventSubscriber());

		return $entityManager;
	}

	private static function createConfiguration(array $prefixes, bool $isDevMode): Configuration
	{
		$config = ORMSetup::createConfiguration($isDevMode);

		$config->setMetadataDriverImpl(new SimplifiedXmlDriver($prefixes));

		return $config;
	}
}

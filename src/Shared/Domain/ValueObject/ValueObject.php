<?php

declare(strict_types=1);

namespace Verifarma\Shared\Domain\ValueObject;

interface ValueObject
{
	/**
	 * @return string|int|float
	 */
	public function value(): mixed;
}

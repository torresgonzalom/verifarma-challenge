<?php

declare(strict_types=1);

namespace Verifarma\Shared\Domain\ValueObject;

use Ramsey\Uuid\Uuid as RamseyUuid;
use Verifarma\Shared\Domain\ValueObject\Exception\InvalidUuid;

abstract readonly class Uuid extends StringValueObject
{
	protected function validateValue(string $value): void
	{
		if (!RamseyUuid::isValid($value)) {
			throw new InvalidUuid();
		}
	}
}

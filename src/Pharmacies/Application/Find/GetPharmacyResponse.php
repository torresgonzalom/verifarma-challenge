<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Find;

use Verifarma\Pharmacies\Domain\Pharmacy;
use Verifarma\Shared\Domain\Bus\Query\Response;

final readonly class GetPharmacyResponse implements Response
{
	private function __construct(public array $pharmacy) {}

	public static function create(Pharmacy $pharmacy): self
	{
		return new self([
			'id' => $pharmacy->getId()->value(),
			'name' => $pharmacy->getName()->value(),
			'address' => $pharmacy->getAddress()->value(),
			'latitude' => $pharmacy->getCoordinates()->getLatitude()->value(),
			'longitude' => $pharmacy->getCoordinates()->getLongitude()->value(),
		]);
	}
}

<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Find;

use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Shared\Domain\Bus\Query\QueryHandler;

final readonly class GetPharmacyQueryHandler implements QueryHandler
{
	public function __construct(private PharmacyFinder $pharmacyFinder) {}

	public function __invoke(GetPharmacyQuery $query): GetPharmacyResponse
	{
		$id = PharmacyId::create($query->id);

		$pharmacy = ($this->pharmacyFinder)($id);

		return GetPharmacyResponse::create($pharmacy);
	}
}

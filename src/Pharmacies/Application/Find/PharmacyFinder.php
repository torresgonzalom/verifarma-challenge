<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Find;

use Verifarma\Pharmacies\Domain\Exception\PharmacyNotFound;
use Verifarma\Pharmacies\Domain\Pharmacy;
use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Pharmacies\Domain\PharmacyRepository;

final class PharmacyFinder
{
	public function __construct(private PharmacyRepository $repository) {}

	public function __invoke(PharmacyId $pharmacyId): Pharmacy
	{
		$pharmacy = $this->repository->find($pharmacyId);

		if (empty($pharmacy)) {
			throw new PharmacyNotFound($pharmacyId);
		}

		return $pharmacy;
	}
}

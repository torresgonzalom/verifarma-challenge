<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Find;

use Verifarma\Pharmacies\Domain\Coordinates;
use Verifarma\Pharmacies\Domain\Latitude;
use Verifarma\Pharmacies\Domain\Longitude;
use Verifarma\Shared\Domain\Bus\Query\QueryHandler;

final readonly class GetNearestPharmacyQueryHandler implements QueryHandler
{
	public function __construct(private NearestPharmacyFinder $pharmacyFinder) {}

	public function __invoke(GetNearestPharmacyQuery $query): GetPharmacyResponse
	{
		$latitude = Latitude::create($query->latitude);
		$longitude = Longitude::create($query->longitude);

		$pharmacy = ($this->pharmacyFinder)(Coordinates::create($latitude, $longitude));

		return GetPharmacyResponse::create($pharmacy);
	}
}

<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Find;

use Verifarma\Shared\Domain\Bus\Query\Query;

final readonly class GetNearestPharmacyQuery implements Query
{
	public function __construct(public float $latitude, public float $longitude) {}
}

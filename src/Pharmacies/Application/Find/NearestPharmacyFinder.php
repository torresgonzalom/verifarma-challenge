<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Find;

use Verifarma\Pharmacies\Domain\Coordinates;
use Verifarma\Pharmacies\Domain\Pharmacy;
use Verifarma\Pharmacies\Domain\PharmacyRepository;

final readonly class NearestPharmacyFinder
{
	public function __construct(private PharmacyRepository $repository) {}

	public function __invoke(Coordinates $coordinates): Pharmacy
	{
		return $this->repository->findNearest($coordinates);
	}
}

<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Find;

use Verifarma\Shared\Domain\Bus\Query\Query;

final readonly class GetPharmacyQuery implements Query
{
	public function __construct(public string $id) {}
}

<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Create;

use Verifarma\Pharmacies\Domain\Address;
use Verifarma\Pharmacies\Domain\Coordinates;
use Verifarma\Pharmacies\Domain\Exception\PharmacyIdAlreadyExists;
use Verifarma\Pharmacies\Domain\Latitude;
use Verifarma\Pharmacies\Domain\Longitude;
use Verifarma\Pharmacies\Domain\Pharmacy;
use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Pharmacies\Domain\PharmacyName;
use Verifarma\Pharmacies\Domain\PharmacyRepository;

final class PharmacyCreator
{
	public function __construct(private readonly PharmacyRepository $repository) {}

	public function __invoke(
		PharmacyId $id,
		PharmacyName $name,
		Address $address,
		Latitude $latitude,
		Longitude $longitude,
	): void {
		$this->pharmacyIdNotExists($id);

		$pharmacy = Pharmacy::create($id, $name, $address, Coordinates::create($latitude, $longitude));

		$this->repository->save($pharmacy);
	}

	private function pharmacyIdNotExists(PharmacyId $id): void
	{
		$pharmacy = $this->repository->find($id);

		if (!empty($pharmacy)) {
			throw new PharmacyIdAlreadyExists($id);
		}
	}
}

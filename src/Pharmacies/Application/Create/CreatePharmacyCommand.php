<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Create;

use Verifarma\Shared\Domain\Bus\Command\Command;

final readonly class CreatePharmacyCommand implements Command
{
	public function __construct(
		public string $id,
		public string $name,
		public string $address,
		public float $latitude,
		public float $longitude,
	) {}
}

<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Application\Create;

use Verifarma\Pharmacies\Domain\Address;
use Verifarma\Pharmacies\Domain\Latitude;
use Verifarma\Pharmacies\Domain\Longitude;
use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Pharmacies\Domain\PharmacyName;
use Verifarma\Shared\Domain\Bus\Command\CommandHandler;

final class CreatePharmacyCommandHandler implements CommandHandler
{
	public function __construct(private readonly PharmacyCreator $pharmacyCreator) {}

	public function __invoke(CreatePharmacyCommand $command): void
	{
		($this->pharmacyCreator)(
			PharmacyId::create($command->id),
			PharmacyName::create($command->name),
			Address::create($command->address),
			Latitude::create($command->latitude),
			Longitude::create($command->longitude),
		);
	}
}

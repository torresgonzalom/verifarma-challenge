<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Infrastructure\Persistence\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Jsor\Doctrine\PostGIS\Types\GeometryType;
use Verifarma\Pharmacies\Domain\Coordinates;
use Verifarma\Pharmacies\Domain\Latitude;
use Verifarma\Pharmacies\Domain\Longitude;
use Verifarma\Shared\Infrastructure\Doctrine\Dbal\DoctrineCustomType;

final class CoordinatesType extends GeometryType implements DoctrineCustomType
{
	protected function typeClassName(): string
	{
		return Coordinates::class;
	}

	public static function customTypeName(): string
	{
		return 'coordinates';
	}

	public function convertToPHPValue($value, AbstractPlatform $platform)
	{
		$coordinates = explode(' ', substr($value, 6, -1));

		return Coordinates::create(
			Latitude::create((float) $coordinates[0]),
			Longitude::create((float) $coordinates[1]),
		);
	}

	public function convertToDatabaseValue($value, AbstractPlatform $platform): string
	{
		/** @var Coordinates $value */
		return "POINT({$value->getLatitude()->value()} {$value->getLongitude()->value()})";
	}
}

<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Infrastructure\Persistence\Doctrine;

use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Shared\Infrastructure\Doctrine\Dbal\UuidType;

final class PharmacyIdType extends UuidType
{
	protected function typeClassName(): string
	{
		return PharmacyId::class;
	}
}

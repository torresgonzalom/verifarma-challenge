<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Infrastructure\Persistence;

use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Verifarma\Pharmacies\Domain\Coordinates;
use Verifarma\Pharmacies\Domain\Pharmacy;
use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Pharmacies\Domain\PharmacyRepository;
use Verifarma\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class DoctrinePharmacyRepository extends DoctrineRepository implements PharmacyRepository
{
	public function save(Pharmacy $pharmacy): void
	{
		$this->persist($pharmacy);
	}

	public function findNearest(Coordinates $coordinates): Pharmacy
	{
		$rsm = new ResultSetMappingBuilder($this->entityManager());
		$rsm->addRootEntityFromClassMetadata(Pharmacy::class, 'p');

		$sql = $this->buildFindNearestSql($coordinates);

		$query = $this->entityManager()->createNativeQuery($sql, $rsm);

		return $query->getSingleResult();
	}

	private function buildFindNearestSql(Coordinates $coordinates): string
	{
		$sql = 'SELECT p.id, p.address, p.name, ST_AsEWKT(coordinates) AS coordinates '
			. 'FROM pharmacies p ORDER '
			. "BY p.coordinates <-> ST_AsEWKT('POINT(%f %f)') LIMIT 1";

		return sprintf($sql, $coordinates->getLatitude()->value(), $coordinates->getLongitude()->value());
	}

	public function find(PharmacyId $id): ?Pharmacy
	{
		return $this->repository(Pharmacy::class)->find($id);
	}
}

<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain\Exception;

use Verifarma\Shared\Domain\DomainError;

final class InvalidLatitude extends DomainError
{
	public function errorCode(): string
	{
		return 'invalid_latitude';
	}

	public function errorMessage(): string
	{
		return 'Latitude is invalid';
	}

	public function errorDescription(): string
	{
		return 'Latitude must be greater than or equal to -90 and less than or equal to 90';
	}
}

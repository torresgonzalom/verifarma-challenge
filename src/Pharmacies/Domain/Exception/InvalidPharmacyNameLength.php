<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain\Exception;

use Verifarma\Shared\Domain\DomainError;

final class InvalidPharmacyNameLength extends DomainError
{
	public function errorCode(): string
	{
		return 'invalid_pharmacy_name_length';
	}

	public function errorMessage(): string
	{
		return 'Pharmacy Name is too long';
	}

	public function errorDescription(): string
	{
		return 'The length of Pharmacy Name must be less than or equal to 60';
	}
}

<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain\Exception;

use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Shared\Domain\DomainError;

final class PharmacyIdAlreadyExists extends DomainError
{
	public function __construct(private readonly PharmacyId $id)
	{
		parent::__construct();
	}

	public function errorCode(): string
	{
		return 'pharmacy_id_already_exists';
	}

	public function errorMessage(): string
	{
		return 'Pharmacy id already exists';
	}

	public function errorDescription(): string
	{
		return sprintf('Pharmacy with id %s already exists', $this->id->value());
	}
}

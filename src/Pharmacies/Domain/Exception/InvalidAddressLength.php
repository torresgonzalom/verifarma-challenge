<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain\Exception;

use Verifarma\Shared\Domain\DomainError;

final class InvalidAddressLength extends DomainError
{
	public function errorCode(): string
	{
		return 'invalid_address_length';
	}

	public function errorMessage(): string
	{
		return 'Address is too long';
	}

	public function errorDescription(): string
	{
		return 'The length of address must be less than or equal to 255';
	}
}

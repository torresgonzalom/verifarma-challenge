<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain\Exception;

use Verifarma\Shared\Domain\DomainError;

final class InvalidLongitude extends DomainError
{
	public function errorCode(): string
	{
		return 'invalid_longitude';
	}

	public function errorMessage(): string
	{
		return 'Longitude is invalid';
	}

	public function errorDescription(): string
	{
		return 'Longitude must be greater than or equal to -180 and less than or equal to 180';
	}
}

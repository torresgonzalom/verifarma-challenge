<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain\Exception;

use Verifarma\Pharmacies\Domain\PharmacyId;
use Verifarma\Shared\Domain\DomainError;

final class PharmacyNotFound extends DomainError
{
	public function __construct(private readonly PharmacyId $id)
	{
		parent::__construct();
	}

	public function errorCode(): string
	{
		return 'pharmacy_not_found';
	}

	public function errorMessage(): string
	{
		return 'Pharmacy not found';
	}

	public function errorDescription(): string
	{
		return sprintf('Pharmacy with id %s not found', $this->id->value());
	}
}

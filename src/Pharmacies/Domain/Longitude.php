<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain;

use Verifarma\Pharmacies\Domain\Exception\InvalidLongitude;
use Verifarma\Shared\Domain\ValueObject\FloatValueObject;

final readonly class Longitude extends FloatValueObject
{
	protected function validateValue(float $value): void
	{
		if (!($value >= -180.0) || !($value <= 180.0)) {
			throw new InvalidLongitude();
		}
	}
}

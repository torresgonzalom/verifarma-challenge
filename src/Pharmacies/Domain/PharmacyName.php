<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain;

use Verifarma\Pharmacies\Domain\Exception\InvalidPharmacyNameLength;
use Verifarma\Shared\Domain\ValueObject\StringValueObject;

final readonly class PharmacyName extends StringValueObject
{
	public function validateValue(string $value): void
	{
		if (strlen($value) > 60) {
			throw new InvalidPharmacyNameLength();
		}
	}
}

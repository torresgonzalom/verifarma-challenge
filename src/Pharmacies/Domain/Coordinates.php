<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain;

final readonly class Coordinates
{
	private function __construct(private Latitude $latitude, private Longitude $longitude) {}

	public static function create(Latitude $latitude, Longitude $longitude): self
	{
		return new self($latitude, $longitude);
	}

	public function getLatitude(): Latitude
	{
		return $this->latitude;
	}

	public function getLongitude(): Longitude
	{
		return $this->longitude;
	}
}

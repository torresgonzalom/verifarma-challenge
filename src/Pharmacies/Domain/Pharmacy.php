<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain;

final class Pharmacy
{
	private function __construct(
		private PharmacyId $id,
		private PharmacyName $name,
		private Address $address,
		private Coordinates $coordinates,
	) {}

	public static function create(
		PharmacyId $id,
		PharmacyName $name,
		Address $address,
		Coordinates $coordinates,
	): self {
		return new self($id, $name, $address, $coordinates);
	}

	public function getId(): PharmacyId
	{
		return $this->id;
	}

	public function getName(): PharmacyName
	{
		return $this->name;
	}

	public function getAddress(): Address
	{
		return $this->address;
	}

	public function getCoordinates(): Coordinates
	{
		return $this->coordinates;
	}
}

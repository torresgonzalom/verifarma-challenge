<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain;

use Verifarma\Shared\Domain\ValueObject\Uuid;

final readonly class PharmacyId extends Uuid {}

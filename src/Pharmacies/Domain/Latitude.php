<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain;

use Verifarma\Pharmacies\Domain\Exception\InvalidLatitude;
use Verifarma\Shared\Domain\ValueObject\FloatValueObject;

final readonly class Latitude extends FloatValueObject
{
	protected function validateValue(float $value): void
	{
		if (!($value >= -90.0) || !($value <= 90.0)) {
			throw new InvalidLatitude();
		}
	}
}

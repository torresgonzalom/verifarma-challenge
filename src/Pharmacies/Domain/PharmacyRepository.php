<?php

declare(strict_types=1);

namespace Verifarma\Pharmacies\Domain;

interface PharmacyRepository
{
	public function save(Pharmacy $pharmacy): void;
	public function findNearest(Coordinates $coordinates): Pharmacy;
	public function find(PharmacyId $id): ?Pharmacy;
}

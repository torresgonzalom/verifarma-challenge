FROM php:8.2-fpm-alpine as php

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN apk add --no-cache --update postgresql-dev

RUN docker-php-ext-install pdo pdo_pgsql

FROM php as php-dev

RUN apk add --no-cache --update linux-headers $PHPIZE_DEPS

RUN pecl install xdebug-3.2.1 \
  && docker-php-ext-enable xdebug

COPY ./etc/xdebug/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

USER 1000:1000
